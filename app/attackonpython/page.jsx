import React from "react";
import aopImage from "/public/assets/projects/aopImage.png";
import Image from "next/image";
import { RiRadioButtonFill } from "react-icons/ri"
import Link from "next/link";

const attackonpython = () => {
  return (
    <div className="w-full">
      <div className="w-screen h-[30vh] lg:h-[40vh] relative">
        <div className="absolute top-0 left-0 w-full h-[30vh] lg:h-[40vh] bg-black/80 z-10" />
        <Image
          className="absolute z-1"
          layout="fill"
          objectFit="cover"
          src={aopImage}
          alt="/"
        />
        <div className="absolute top-[70%] max-w-[1240px] w-full left-[50%] right-[50%] translate-x-[-50%] translate-y-[-50%] text-white z-10 p-2">
          <h2 className="py-2">Attack on Python</h2>
          <h3>React.js | FastAPI | PostgresQL</h3>
        </div>
      </div>

      <div className="max-w-[1240px] mx-auto p-2 grid md:grid-cols-5 gap-8 pt-8">
        <div className="col-span-4">
          <p>Project</p>
          <h2>Overview</h2>
          <p>
            {" "}
            Attack on Python is a quiz-formatted game where beginner devs can
            test their Python knowledge against the evil CPU Pythons by
            answering their coding questions. I designed custom React Hooks,
            which effectively managed the state of the game from the
            playerHealth, opponentHealth, the stats managing the interactions
            between the player and the opponent after a question is answered, as
            well as other aspects of the main battle sequence. I also created a
            modularized structure for the React components that were responsible
            for the Battle Sequence and ensured that the React components
            interacted seamlessly with the fastAPI requests from the back-end
            database in PostgresQL. This was essential for maintaining a
            cohesive flow between the frontend and backend, ensuring smooth data
            exchange and user interactions. One aspect of this project that I
            really enjoyed was creating the dialogue hook that looped through
            the array of characters in the dialogue to ensure that the text
            outputed for the questions were displayed in a format similar to old
            RPG games where each character is submitted to display with a fixed
            delay.
          </p>
          <Link href="https://gitlab.com/attack-on-python/attack-on-python">
          <button className="px-8 py-2 mt-4 mr-8">Code</button>
          </Link>
          <Link href="https://attack-on-python.gitlab.io/attack-on-python/">
          <button className="px-8 py-2 mt-4">Demo</button>
          </Link>
        </div>
        <div className="col-span-4 md:col-span-1 shadow-xl shadow-gray-400 rounded-xl p-4">
          <div className="p-2">
            <p className="text-center font-bold pb-2">Technologies</p>
            <div>
              <p className="text-gray-600 py-2 flex items-center"> < RiRadioButtonFill /> React </p>
              <p className="text-gray-600 py-2 flex items-center"> < RiRadioButtonFill /> FastAPI </p>
              <p className="text-gray-600 py-2 flex items-center"> < RiRadioButtonFill /> JavaScript </p>
              <p className="text-gray-600 py-2 flex items-center"> < RiRadioButtonFill /> PostgresQL </p>
              <p className="text-gray-600 py-2 flex items-center"> < RiRadioButtonFill /> Docker </p>
              <p className="text-gray-600 py-2 flex items-center"> < RiRadioButtonFill /> CI/CD </p>
            </div>
          </div>
        </div>
        <Link href="/#projects">
          <p className="underline cursor-pointer">Back</p>
        </Link>
      </div>
    </div>
  );
};

export default attackonpython;
