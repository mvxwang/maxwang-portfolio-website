import React from "react";
import { AiOutlineMail } from "react-icons/ai";
import { BsFillPersonLinesFill } from "react-icons/bs";
import { FaGitlab, FaLinkedinIn } from "react-icons/fa";
import Link from "next/link";

const Main = () => {
  return (
    <div id="home" className="w-full h-screen text-center">
      <div className="max-w-[1240px] w-full h-full mx-auto p-2 flex justify-center items-center">
        <div>
          <p className="uppercase text-sm tracking-widest text-gray-600">
            {" "}
            LET&apos;s ENGINEER SOMETHING REMARKABLE TOGETHER
          </p>
          <h1 className="py-4 text-gray-700">
            Hello, I&apos;m <span className="text-[#00e987]"> Max</span>
          </h1>
          <h1 className="py-2 text-gray-700">A Full-Stack Software Engineer</h1>
          <p className="py-4 text-gray-600 max-w-[70%] m-auto">
            My background in the physical and life sciences, combined with my
            technical expertise in software development, equips me to approach
            problem-solving with a unique perspective, enabling me to develop
            creative and streamlined solutions that can make a profound impact
            in the tech industry.
          </p>
          <div className="flex items-center justify-between max-w-[330px] m-auto py-4">
            <div className="rounded-full shadow-lg shadow-gray-400 p-6 cursor-pointer hover:scale-110 ease-in duration-300">
              <Link href="https://www.linkedin.com/in/maxwangasu/">
                <FaLinkedinIn />
              </Link>
            </div>
            <div className="rounded-full shadow-lg shadow-gray-400 p-6 cursor-pointer hover:scale-110 ease-in duration-300">
              <Link href="https://gitlab.com/mvxwang">
                <FaGitlab />
              </Link>
            </div>
            <div className="rounded-full shadow-lg shadow-gray-400 p-6 cursor-pointer hover:scale-110 ease-in duration-300">
              <a href="mailto:mwang171@asu.edu">
                <AiOutlineMail />
              </a>
            </div>
            <div className="rounded-full shadow-lg shadow-gray-400 p-6 cursor-pointer hover:scale-110 ease-in duration-300">
              <a href="/assets/Max-Wang-Resume-2024.pdf" download>
                <BsFillPersonLinesFill />
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Main;
