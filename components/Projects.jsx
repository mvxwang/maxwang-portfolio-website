import React from "react";
import aopImage from "../public/assets/projects/aopImage.png";
import ProjectItem from "./ProjectItem";

const Projects = () => {
  return (
    <div id="projects" className="w-full">
      <div className="max-w[1240px] mx-auto px-2 py-16">
        <p className="text-xl tracking-widest uppercase text-[#00e987] mt-8">
          Projects
        </p>
        <h2 className="py-4 text-gray-700">What I&apos;ve Built</h2>
        <div className="grid md:grid-cols-2 gap-8">
          <ProjectItem
            title="Attack On Python"
            backgroundImg={aopImage}
            projectUrl="/attackonpython"
            projectDetails=""
          />

        </div>
      </div>
    </div>
  );
};

export default Projects;
