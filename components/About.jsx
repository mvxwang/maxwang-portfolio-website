import React from "react"
import Image from "next/image"
import Link from "next/link"

const About = () => {
  return (
    <div id="about" className="w-full md:h-screen p-2 flex items-center py-16">
        <div className="max-w[1240px] m-auto md:grid grid-cols-3 gap-8">
            <div className="col-span-2">
                <p className="uppercase text-xl tracking-widest text-[#00e987] mt-8">About Me</p>
                <h2 className="py-4 text-gray-700">Who I Am</h2>
                <p className="py-2 text-gray-600">
                    I&apos;m a Full-Stack Software Engineer with a diverse scientific background.
                    My academic journey began in the Life & Physical sciences, from physics, to organic chemistry, molecular biology, and neuroscience.
                    I immersed myself in a dynamic range of scientific and mathematical disciplines. While my courses differed immensely in subject material,
                    what left a lasting impact on me, was the shared essence of the scientific process -- the methodical approach to scientific theory and its
                    practical applications to solve problems in the real world. I spent some time in applied research at an organic chemistry laboratory at ASU.
                    My team and I were on a mission to develop novel base metal catalysts that could rival or surpass current industry standards in use such as cobalt and palladium.
                    These catalysts are not only expensive, but also environmentally unfriendly to procure. Seeing the potential of optimization and obvious drawbacks of the usage of these materials
                    was an inspiring challenge for me and deeply resonated with my natural passion for problem-solving. 
                </p>
                <p className="py-2 text-gray-600">
                    This experience highlighted my love for tackling complex challenges and
                    finding creative solutions to real-world problems. As such, this experience was pivotal in shaping my decision to transition to Software Engineering, which was further fueled by my innate fascinations with technology and its applications. 
                    The path from the scientfic world to software engineering was a natural evolution for me, reflecting my adaptability, capacity to learn, and my unyielding commitment to make a difference through innovation.
                </p>
                <Link href="/#projects">
                <p className="py-2 text-gray-600 underline cursor-pointer">
                    Check out some of my latest projects here.
                </p>
                </Link>
            </div>
            <div className="w-full h-auto m-auto shadow-xl shadow-gray-400 rounded-xl flex items-center justify-center p-4 hover:scale-105 ease-in duration-300">
                <Image 
                    className="rounded-xl" 
                    src="/assets/aboutMePhoto.png" 
                    alt="/" 
                    width={400} 
                    height={700}
                />
            </div>
            <div></div>
        </div>
    </div>
  )
}

export default About